# Determine if a user input is a leap year or not

# [solution 1]
# year_input = input("Enter a year: ")

# if not year_input.isdigit() or int(year_input) <= 0 :
#     print("[Try again] Input is not a number, zero, or negative.")

# else :
#     year_input = int(year_input)

#     if year_input % 4 == 0 and year_input % 100 != 0 :
#         print(f"{year_input} is a leap year")

#     else :
#         print(f"{year_input} is not a leap year")

# [solution 2]
run = True;

while run :

    year_input = input("Enter a year: ")
    if not year_input.isdigit() or int(year_input) <= 0 :
        print("[Try again] Input is not a number, zero, or negative.\n")
        continue

    year_input = int(year_input)

    if year_input % 4 == 0 and year_input % 100 != 0 :
        print(f"{year_input} is a leap year")

    else :
        print(f"{year_input} is not a leap year")

    run = False;


# Create a grid of asterisk from 2 user inputs (row and col)
row = int(input("Input row count: "))
col = int(input("Input column count: "))

for r in range(row):
    print('*' * col)

